<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Ejemplo con el cargador automático de composer
require '../vendor/autoload.php';

//Crear una instancia y pasar true para permitir las excepciones
$mail = new PHPMailer(true);

try {
    //Configuración del servidor
    $mail->SMTPDebug = SMTP::DEBUG_SERVER;             //Habilitar los mensajes de depuración
    $mail->isSMTP();                                   //Enviar usando SMTP
    $mail->Host       = 'mail.llantascs.com';            //Configurar el servidor SMTP
    $mail->SMTPAuth   = true;                          //Habilitar autenticación SMTP
    $mail->Username   = 'notificaciones@llantascs.com';            //Nombre de usuario SMTP
    $mail->Password   = 'tRDmrltRFATu';                      //Contraseña SMTP
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;   //Habilitar el cifrado TLS
    $mail->Port       = 465;                           //Puerto TCP al que conectarse; use 587 si configuró `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

    //Emisor
    $mail->setFrom('notificaciones@llantascs.com', 'Llantas de Calidad Star');

    //Destinatarios
    $mail->addAddress('alisgar9@gmail.com', 'alicia alanis');     //Añadir un destinatario, el nombre es opcional
    //Nombre opcional
    $mail->isHTML(true);                         //Establecer el formato de correo electrónico en HTMl
    $mail->Subject = 'Asunto';             
    $mail->Body    = '¡Este es el cuerpo del mensaje HTML <b>en negrita!</b>';
    $mail->AltBody = 'Este es el cuerpo en texto sin formato para clientes de correo que no son HTML';

    $mail->send();    //Enviar correo eletrónico
    echo 'El mensaje ha sido enviado';
} catch (Exception $e) {
    echo "No se pudo enviar el mensaje. Error de correo: {$mail->ErrorInfo}";
}