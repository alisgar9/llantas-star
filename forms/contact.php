<?php
error_reporting(0);
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Ejemplo con el cargador automático de composer
require '../vendor/autoload.php';


// Replace contact@example.com with your real receiving email address
  $receiving_email_address = 'ventas@llantascs.com';


  $name = $_POST['name'];
  $email= $_POST['email'];
  $subject= 'Mensaje Enviado desde WEB';
  $message= $_POST['message'];
  

//Crear una instancia y pasar true para permitir las excepciones
$mail = new PHPMailer(true);

//Configuración del servidor
    $mail->SMTPDebug = SMTP::DEBUG_SERVER;             //Habilitar los mensajes de depuración
    $mail->isSMTP();                                   //Enviar usando SMTP
    $mail->Host       = 'mail.llantascs.com';            //Configurar el servidor SMTP
    $mail->SMTPAuth   = true;                          //Habilitar autenticación SMTP
    $mail->Username   = 'notificaciones@llantascs.com';            //Nombre de usuario SMTP
    $mail->Password   = 'tRDmrltRFATu';                      //Contraseña SMTP
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;   //Habilitar el cifrado TLS
    $mail->Port       = 465;                           //Puerto TCP al que conectarse; use 587 si configuró `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

    //Emisor
    $mail->setFrom('notificaciones@llantascs.com', 'Llantas de Calidad Star');

    //Destinatarios
    $mail->addAddress($receiving_email_address, 'Llantas de Calidad Star');     //Añadir un destinatario, el nombre es opcional
    //Nombre opcional
    $mail->isHTML(true);                         //Establecer el formato de correo electrónico en HTMl
    $mail->Subject = $subject;             
    $mail->Body    = 'Nombre: <b>'.$name.'</b><br> Email: <b>'.$email.'</b><br> Mensaje: <b>'.$message.'</b>';
    $mail->AltBody = 'Nombre: '.$name.' Email: '.$email.'  Mensaje: '.$message ;


    if($mail->send()){
        return json_encode([
                            'error'=> 0,
                            'message' => 'El mensaje ha sido enviado'], 201);
    }
    else{
        return json_encode([
                            'error'=> 1,
                            'message' => 'No se pudo enviar el mensaje.'], 201);
    }
    

?>
